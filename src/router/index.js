import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/Login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path:'/index',
    name:'index',
    component: () => import(/* webpackChunkName: "index" */ '../views/index.vue'),
    children:[
      {
        path:'roles',
        name:'roles',
        component: () => import(/* webpackChunkName: "roles" */ '../views/roles/Roles.vue'),
      },
      {
        path:'users',
        name:'users',
        component: () => import(/* webpackChunkName: "users" */ '../views/users/User.vue'),
      },
      {
        path:'rights',
        name:'rights',
        component: () => import(/* webpackChunkName: "rights" */ '../views/rights/Rights.vue'),
      },
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
