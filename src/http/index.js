import axios from 'axios'
import { Message } from 'element-ui'
import store from '../store'
import router from '../router'
import { login } from './api'

const instance = axios.create({
    baseURL: "http://localhost:8888/api/private/v1/"
})

//请求拦截器
instance.interceptors.request.use((config)=>{
    if(store.state.token){
        config.headers['Authorization'] = store.state.token
    }
    return config
},(error)=>{
    return Promise.reject(error)
})

//相应拦截器

instance.interceptors.response.use((response)=>{
    if (response.data.meta.status === 400) {
        router.push({
            name:'login'
        })
    }
    return response
},(error)=>{
    return Promise.reject(error)
})

export function http(url, method, data, params) {
    return new Promise((resolve,reject)=>{
        instance({
            url,
            method,
            data,
            params
        })
        .then(res => {
            if((res.status >= 200 && res.status < 300) || res.status === 304){
                if((res.data.meta.status >= 200 && res.data.meta.status < 300) || res.data.meta.status === 304){
                    // Message({
                    //     showClose: true,
                    //     message: '欢迎回来，' + res.data.data.username,
                    //     type: 'success'
                    // })
                    resolve(res.data.data)
                }else{
                    Message({
                        showClose: true,
                        message: res.data.meta.status,
                        type: 'error'
                    })
                    reject(res)
                }
            }else{
                reject(res)
            }
        }).catch(err => {
            Message({
                showClose: true,
                message: err.message,
                type: 'error'
            })
            reject(err)
        })
    })
}


